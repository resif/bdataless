Ce dépot contient dans le répertoire bynet les métadonnées des réseaux RESIF au formatt SEED dataless : un fichier par NETWORK/STATION.
Dans le cas du réseau FR, on trouve de plus dans bynet/FR un fichier pour les canaux de la composante acélérométrique s'ils s'en trouve, et 
fichier pour les canaux de la composante vélocimétriques, en plus du volume dataless complet pour tous les canaux de chaque station.


Chaque volume SEED dataless est obtenu par la séquance suivante :

Pour chaque couple : NETWORK/STATION :
1) récupéreration au noeud B de la réponse au format stationXML par wget
(et par type de canal dans le cas du réseau FR)
2) conversion du fhcier stationXML en utilisant l'outil IRIS : stationxml-seed-converter-2.0.10-SNAPSHOT.jar (https://github.com/iris-edu/stationxml-seed-converter/releases/tag/2.0.10)

Environnement pour la conversion :
linux FC31
en_US.UTF-8
java version "11.0.6" 2020-01-14 LTS
